import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAIkHMpSpoYlKhfqLy4Usxb-A7F7_-9P8M",
  authDomain: "netflix-clone-29f51.firebaseapp.com",
  databaseURL: "https://netflix-clone-29f51.firebaseio.com",
  projectId: "netflix-clone-29f51",
  storageBucket: "netflix-clone-29f51.appspot.com",
  messagingSenderId: "485767500371",
  appId: "1:485767500371:web:331b6f15a068f07c26aca6",
  measurementId: "G-SDQ5P2S2SF",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export { db };
