import React, { useEffect, useRef, useState } from "react";
import "./Row.css";
import axios from "./axios";
import * as config from "./config";
import YouTube from "react-youtube";
import movieTrailer from "movie-trailer";

const Row = (props) => {
  const { title, fetchUrl, isLargeRow } = props;

  const [movies, setMovies] = useState([]);
  const [trailerUrl, setTrailerUrl] = useState("");

  const refYoutube = useRef(null);

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(fetchUrl);
      setMovies(request.data.results);
      return request;
    }
    fetchData();
  }, [fetchUrl]);

  const opts = {
    height: "390",
    width: "100%",
    playerVars: {
      autoplay: 1,
    },
  };

  const handleMovie = (movie) => {
    if (trailerUrl) {
      setTrailerUrl("");
    }
    movieTrailer(movie?.name || movie?.title || "")
      .then((url) => {
        const urlParams = new URLSearchParams(new URL(url).search);
        setTrailerUrl(urlParams.get("v"));
      })
      .catch((error) => {
        alert("Trailer not found");
      });
  };

  useEffect(() => {
    refYoutube?.current?.scrollIntoView();
  }, [trailerUrl]);

  return (
    <div className="row">
      <h2>{title}</h2>
      <div className={`row__posters`}>
        {movies?.map((movie, index) => {
          return (
            <img
              key={index}
              onClick={() => handleMovie(movie)}
              className={`row__poster ${isLargeRow && "row__posterLarge"}`}
              src={`${config.config.imgBaseURL}${
                isLargeRow ? movie.poster_path : movie.backdrop_path
              }`}
              alt=""
            />
          );
        })}
      </div>
      <div ref={refYoutube}>
        {trailerUrl && <YouTube videoId={trailerUrl} opts={opts}></YouTube>}
      </div>
    </div>
  );
};

export default Row;
