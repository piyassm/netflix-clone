import axios from "axios";
import * as config from "./config";

const instance = axios.create({
  baseURL: config.config.baseURL,
});

export default instance;
