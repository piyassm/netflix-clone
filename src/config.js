export const config = {
  baseURL: "https://api.themoviedb.org/3",
  imgBaseURL: "https://image.tmdb.org/t/p/original",
  imgBaseURL500: "https://image.tmdb.org/t/p/w500",
};
